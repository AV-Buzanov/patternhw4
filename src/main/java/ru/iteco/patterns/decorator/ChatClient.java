package ru.iteco.patterns.decorator;

import ru.iteco.patterns.proxy.NamesReplacer;
import ru.iteco.patterns.proxy.TextScrambler;

public interface ChatClient {
    @NamesReplacer
    Message readMessage();

    @TextScrambler
    void sendMessage(Message message);
}
