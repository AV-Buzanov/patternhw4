package ru.iteco.patterns.decorator;

public class ChatNamesReplacerDecorator extends AbstractChatDecorator implements ChatClient {
    public ChatNamesReplacerDecorator(ChatClient chatClient) {
        super(chatClient);
    }

    @Override
    public Message readMessage() {
        Message message = super.readMessage();
        doCodeNames(message);
        return message;
    }

    @Override
    public void sendMessage(Message message) {
        if (message != null) {
            doCodeNames(message);
        }
        super.sendMessage(message);
    }

    private void doCodeNames(Message message) {
        if (message.getAuthor() != null) {
            message.setAuthor(String.valueOf(message.getAuthor().hashCode()));
        }
        if (message.getRecipient() != null) {
            message.setRecipient(String.valueOf(message.getRecipient().hashCode()));
        }
    }


}
