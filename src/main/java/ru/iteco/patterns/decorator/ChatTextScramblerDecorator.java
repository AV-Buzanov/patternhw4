package ru.iteco.patterns.decorator;

import java.util.stream.Collectors;

public class ChatTextScramblerDecorator extends AbstractChatDecorator implements ChatClient {
    public ChatTextScramblerDecorator(ChatClient chatClient) {
        super(chatClient);
    }

    @Override
    public Message readMessage() {
        Message message = super.readMessage();
        doScrambleText(message);
        return message;
    }

    @Override
    public void sendMessage(Message message) {
        if (message != null) {
            doScrambleText(message);
        }
        super.sendMessage(message);
    }

    private void doScrambleText(Message message) {
        if (message.getText() != null) {
            String code = message.getText()
                    .chars()
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(":"));
            message.setText(code);
        }
    }
}
