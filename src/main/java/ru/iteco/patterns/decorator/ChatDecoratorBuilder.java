package ru.iteco.patterns.decorator;

public class ChatDecoratorBuilder {
    private ChatClient client;

    public ChatDecoratorBuilder(ChatClient client) {
        this.client = client;
    }

    public ChatDecoratorBuilder withNamesReplacer(){
        client = new ChatNamesReplacerDecorator(client);
        return this;
    }

    public ChatDecoratorBuilder withTextScrambler(){
        client = new ChatTextScramblerDecorator(client);
        return this;
    }

    public ChatClient build(){
        return client;
    }
}
