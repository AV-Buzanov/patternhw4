package ru.iteco.patterns.decorator;

/**
 * Main.
 *
 * @author Aleksey Buzanov
 */
public class Main {


    public static void main(String[] args) {
        ChatClient client = new ChatDecoratorBuilder(new SimpleChat())
                .withNamesReplacer()
                .withTextScrambler()
                .build();
        client.sendMessage(Message.builder()
                .withText("My text")
                .withAuthor("author")
                .withRecipient("Me")
                .build());
        System.out.println("---------------------");
        System.out.println(client.readMessage());
    }
}
