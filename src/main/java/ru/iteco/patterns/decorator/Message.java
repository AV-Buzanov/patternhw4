package ru.iteco.patterns.decorator;

/**
 * Message.
 *
 * @author Aleksey Buzanov
 */
public class Message {
    private String author;
    private String recipient;
    private String text;

    private Message(MessageBuilder builder) {
        this.author = builder.author;
        this.recipient = builder.recipient;
        this.text = builder.text;
    }

    public static MessageBuilder builder() {
        return new MessageBuilder();
    }

    public static class MessageBuilder {
        private String author;
        private String recipient;
        private String text;

        public MessageBuilder withAuthor(String author) {
            this.author = author;
            return this;
        }

        public MessageBuilder withRecipient(String recipient) {
            this.recipient = recipient;
            return this;
        }

        public MessageBuilder withText(String text) {
            this.text = text;
            return this;
        }

        public Message build() {
            return new Message(this);
        }
    }

    public String getAuthor() {
        return author;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getText() {
        return text;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return String.format("Сообщение от: %s,\n Получатель: %s , \n Текст: %s \n",
                getAuthor(), getRecipient(), getText());
    }
}
