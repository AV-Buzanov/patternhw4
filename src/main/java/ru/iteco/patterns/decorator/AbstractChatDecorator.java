package ru.iteco.patterns.decorator;

public class AbstractChatDecorator implements ChatClient {
    protected ChatClient chatClient;

    public AbstractChatDecorator(ChatClient chatClient) {
        this.chatClient = chatClient;
    }

    @Override
    public Message readMessage() {
        if (chatClient!=null){
            return chatClient.readMessage();
        }
        return null;
    }

    @Override
    public void sendMessage(Message message) {
        if (chatClient!=null){
            chatClient.sendMessage(message);
        }
    }
}
