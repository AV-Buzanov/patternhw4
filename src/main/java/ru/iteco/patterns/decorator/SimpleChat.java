package ru.iteco.patterns.decorator;

public class SimpleChat implements ChatClient {
    @Override
    public Message readMessage() {
        return Message.builder()
                .withAuthor("Автор неизвестен")
                .withRecipient("Бузанов А.В")
                .withText("Привет")
                .build();
    }

    @Override
    public void sendMessage(Message message) {
        if (message != null) {
            System.out.printf("Сообщение от: %s,\n Получатель: %s , \n Текст: %s \n",
                    message.getAuthor(), message.getRecipient(), message.getText());
        }
    }
}
