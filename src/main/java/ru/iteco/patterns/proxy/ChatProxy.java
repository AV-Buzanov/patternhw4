package ru.iteco.patterns.proxy;

import ru.iteco.patterns.decorator.ChatClient;
import ru.iteco.patterns.decorator.Message;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ChatProxy implements InvocationHandler {
    protected ChatClient chatClient;

    public ChatProxy(ChatClient chatClient) {
        this.chatClient = chatClient;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        List<Class<? extends Annotation>> annotations = Arrays
                .stream(method.getAnnotations())
                .map(Annotation::annotationType)
                .collect(Collectors.toList());
        if ("sendMessage".equals(method.getName()) && args.length > 0) {
            Message message = (Message) args[0];
            doChanges(annotations, message);
            args[0] = message;
            method.invoke(chatClient, args);
            return Void.TYPE;
        }

        if ("readMessage".equals(method.getName())) {
            Message message = (Message) method.invoke(chatClient, args);
            doChanges(annotations, message);
            return message;
        }
        return method.invoke(chatClient, args);
    }

    private void doChanges(List<Class<? extends Annotation>> annotations, Message message) {
        if (annotations.contains(TextScrambler.class)) {
            doScrambleText(message);
        } else if (annotations.contains(NamesReplacer.class)) {
            doCodeNames(message);
        }
    }

    private void doScrambleText(Message message) {
        if (message.getText() != null) {
            String code = message.getText()
                    .chars()
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(":"));
            message.setText(code);
        }
    }

    private void doCodeNames(Message message) {
        if (message.getAuthor() != null) {
            message.setAuthor(String.valueOf(message.getAuthor().hashCode()));
        }
        if (message.getRecipient() != null) {
            message.setRecipient(String.valueOf(message.getRecipient().hashCode()));
        }
    }
}
