package ru.iteco.patterns.proxy;

import ru.iteco.patterns.decorator.ChatClient;
import ru.iteco.patterns.decorator.Message;
import ru.iteco.patterns.decorator.SimpleChat;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Main.
 *
 * @author Aleksey Buzanov
 */
public class Main {


    public static void main(String[] args) {
        ClassLoader classLoader = SimpleChat.class.getClassLoader();
        Class<?>[] interfaces = SimpleChat.class.getInterfaces();
        InvocationHandler invocationHandler = new ChatProxy(new SimpleChat());
        ChatClient client = (ChatClient) Proxy.newProxyInstance(classLoader, interfaces, invocationHandler);
        client.sendMessage(Message.builder()
                .withText("My text")
                .withAuthor("author")
                .withRecipient("Me")
                .build());
        System.out.println("---------------------");
        System.out.println(client.readMessage());
    }
}
